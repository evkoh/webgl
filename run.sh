#!/usr/bin/env bash

./node_modules/http-server/bin/http-server ./ &

if [ "$(uname)" == "Darwin" ]; then
    echo 'Mac OS X platform';
    # Google Chrome Alias (for macOS)
    google-chrome() {
        open -a "Google Chrome" "http://127.0.0.1:8081/Projet_WebGL_2019.html"
    }
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo 'Linux';
    google-chrome --new-window --incognito "http://127.0.0.1:8081/Projet_WebGL_2019.html"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    echo '32 bitsWindows NT';
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    echo '64 bits Windows NT platform';
fi
