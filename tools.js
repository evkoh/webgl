import * as THREE from './node_modules/three/build/three.module.js';
import {GUI} from './node_modules/three/examples/jsm/libs/dat.gui.module.js';
import {Sky} from './node_modules/three/examples/jsm/objects/Sky.js';
import {SkeletonUtils} from './node_modules/three/examples/jsm/utils/SkeletonUtils.js';
import {OBJLoader} from "./node_modules/three/examples/jsm/loaders/OBJLoader.js";
import {MTLLoader} from './node_modules/three/examples/jsm/loaders/MTLLoader.js';

// Sky
var sky, sunSphere;

// LoadingManager
var manager = new THREE.LoadingManager();

// Nos loaders ( loader d'objet, et loader de material )
var objLoader, mtlLoader;

// On trie un tableau d'objet par ordre de la propriété order de chaque objet
// https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value#1129270
export function orderSort(arrayDatas) {
    return arrayDatas.sort(function (a, b) {
        if (a.order > b.order) {
            return -1 * -1;
        }
        if (b.order > a.order) {
            return 1 * -1;
        }
        return 0 * -1;
    })
}

// view-source:https://threejs.org/examples/webgl_shaders_sky.html ligne 32
export function initSky(scene, renderer, camera) {
    // Ajout du ciel
    sky = new Sky();
    sky.scale.setScalar(450000);
    rotateObject(sky, 90, 90, 90);
    scene.add(sky);

    // Ajout du soleil
    sunSphere = new THREE.Mesh(
        new THREE.SphereBufferGeometry(20000, 16, 8),
        new THREE.MeshBasicMaterial({color: 0xffffff})
    );
    //sunSphere.position.y = -700000;
    sunSphere.visible = true;
    scene.add(sunSphere);

    /// Inteface pour gérer les paramètres
    var effectController = {
        turbidity: 10,
        rayleigh: 2,
        mieCoefficient: 0.005,
        mieDirectionalG: 0.8,
        luminance: 1,
        inclination: 0.49, // elevation / inclination
        azimuth: 0.5182, // Facing front,
        sun: true
    };

    function guiChanged() {
        var distance = 400000;
        var theta = Math.PI * (effectController.inclination - 0.5);
        var phi = 2 * Math.PI * (effectController.azimuth - 0.5);
        sunSphere.position.x = distance * Math.cos(phi);
        sunSphere.position.y = distance * Math.sin(phi) * Math.sin(theta);
        sunSphere.position.z = distance * Math.sin(phi) * Math.cos(theta);
        sunSphere.visible = effectController.sun;

        var uniforms = sky.material.uniforms;
        uniforms["turbidity"].value = effectController.turbidity;
        uniforms["rayleigh"].value = effectController.rayleigh;
        uniforms["mieCoefficient"].value = effectController.mieCoefficient;
        uniforms["mieDirectionalG"].value = effectController.mieDirectionalG;
        uniforms["luminance"].value = effectController.luminance;
        uniforms["sunPosition"].value.copy(sunSphere.position);
        renderer.render(scene, camera);
    }

    var gui = new GUI();
    gui.add(effectController, "turbidity", 1.0, 20.0, 0.1).onChange(guiChanged);
    gui.add(effectController, "rayleigh", 0.0, 4, 0.001).onChange(guiChanged);
    gui.add(effectController, "mieCoefficient", 0.0, 0.1, 0.001).onChange(guiChanged);
    gui.add(effectController, "mieDirectionalG", 0.0, 1, 0.001).onChange(guiChanged);
    gui.add(effectController, "luminance", 0.0, 2).onChange(guiChanged);
    gui.add(effectController, "inclination", 0, 1, 0.0001).onChange(guiChanged);
    gui.add(effectController, "azimuth", 0, 1, 0.0001).onChange(guiChanged);
    gui.add(effectController, "sun").onChange(guiChanged);
    guiChanged();
}

export function tunnel(scene) {
    // Je fais un simple anneau, avec RingGeometry
    // https://threejs.org/docs/#api/en/geometries/RingGeometry
    let geometry = new THREE.RingGeometry(10, 15, 100);

    // Création d'un ShaderMaterial couleur grise pour l'appliquer à ma geometry
    let materialGrey = new THREE.ShaderMaterial({
        uniforms: {},
        fragmentShader: `
void main() {
	gl_FragColor = vec4(0.548,0.565,0.542, 0.0);
}
`,
        vertexShader: `
void main()
{
	vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
	gl_Position = projectionMatrix * modelViewPosition;
}
`,
    });

    // Création d'un ShaderMaterial couleur noir pour l'appliquer à ma geometry
    let materialBlack = new THREE.ShaderMaterial({
        uniforms: {},
        fragmentShader: `
void main() {
	gl_FragColor = vec4(0.0,0.0,0.0, 0.0);
}
`,
        vertexShader: `
void main()
{
	vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
	gl_Position = projectionMatrix * modelViewPosition;
}
`,
    });

    // Je fabrique à Mesh avec la geometry et mon material noir
    let mesh = new THREE.Mesh(geometry, materialBlack);

    // J'effecute une rotation de 90° sur l'axe X
    rotateObject(mesh, 90, 0, 0);

    // Tunnel n°1
    let positionY = 0;
    for (let i = 0; i < 1000; i++) {
        let clone = SkeletonUtils.clone(mesh);
        clone.position.x = -463.10;
        clone.position.z = 10;

        // Un anneau sur 5 est gris
        if (i % 5 == 0) {
            clone.material = materialGrey;
        }
        clone.position.y -= positionY;
        //movable.push(clone);
        scene.add(clone);
        positionY += 0.2;
    }

    // Tunnel n°2
    positionY += 100;
    for (let i = 0; i < 1000; i++) {
        let clone = SkeletonUtils.clone(mesh);
        clone.position.x = -463.10;
        clone.position.z = 10;

        // Un anneau sur 5 est gris
        if (i % 5 == 0) {
            clone.material = materialGrey;
        }
        clone.position.y -= positionY;
        //movable.push(clone);
        scene.add(clone);
        positionY += 0.2;
    }
}

// Fonction pour faire une rotation x, y et/ou z d'un object
export function rotateObject(object, degreeX = 0, degreeY = 0, degreeZ = 0) {
    object.rotateX(THREE.Math.degToRad(degreeX));
    object.rotateY(THREE.Math.degToRad(degreeY));
    object.rotateZ(THREE.Math.degToRad(degreeZ));
}

export function arriveeEnGare(camera, stateTrain) {
    // Je joue le son du frein
    playSound(camera, stateTrain, 'data/sounds/3.mp3', false, 1.0, true);

    // Ralentis dès l'arrivée en gare
    stateTrain.vitesseDuTrain = 1;
    for (let i = 1; i <= 9; i++) {

        // Au bout de (i * 600 millisecondes), j'augmente la propriété vitesseDuTrain donc je ralentis le train
        // parceque vitesseDuTrain est utilisé dans un modulo. Plus vitesseDuTrain est grand plus le train avance lentement
        setTimeout(function () {
            stateTrain.vitesseDuTrain += 1;
        }, i * 600);
    }
}

export function stopEnGare(stateTrain) {
    // Je met le train à l'arrêt
    stateTrain.state = 0;

    // Au bout de 10 secondes, on re-démarre le train
    setTimeout(function () {
        stateTrain.state = 1;
    }, 10 * 1000);
}

export function departDeGare(camera, stateTrain) {
    playSound(camera, stateTrain, 'data/sounds/1.mp3', false, 0.6, true);

    // Accélère dès le départ de la gare
    stateTrain.vitesseDuTrain = 10;
    for (let i = 1; i <= 8; i++) {
        setTimeout(function () {
            stateTrain.vitesseDuTrain -= 1;
        }, i * 300);
    }

    loadCameraLocation(camera, null);
}

export function loadCameraLocation(camera, cameraState) {
    if (cameraState === null) {
        cameraState = '[0.9215173093713822,-0.388337287070052,2.775557561562891e-17,0,0.10991012701552855,0.2608147295980521,0.9591117978651527,0,-0.3724588735798335,-0.8838381233550444,0.2830274884103568,0,-596.8987685351566,-1006.790845361925,311.97251602046117,1]';
    }

    // Je parse le json contenu dans la chaine de caractère cameraState afin d'obtenir un tableau
    // Je soumet ce tableau à la matrix de ma camera via la fonction fromArray()
    camera.matrix.fromArray(JSON.parse(cameraState));

    // https://stackoverflow.com/questions/29221795/serializing-camera-state-in-threejs#29223836
    camera.matrix.decompose(camera.position, camera.quaternion, camera.scale);
    camera.up = new THREE.Vector3(0, 0, 2);

    // https://threejs.org/docs/#api/en/cameras/PerspectiveCamera.updateProjectionMatrix
    camera.updateProjectionMatrix();
}

/**
 * Avance de x rails.
 * Si on dépasse le nombre de rails maximums (taille du tableau d'objets qui contient toutes les rails)
 * Alors on remet le compteur à zéro
 *
 * @param iPositionRailTrainElement
 * @param railsPositions
 * @param iPositionRailTrainElementIndex
 * @param avancementRails
 * @return {*}
 */
export function avancementRail(iPositionRailTrainElement, railsPositions, iPositionRailTrainElementIndex, avancementRails = 1) {
    iPositionRailTrainElement[iPositionRailTrainElementIndex] += avancementRails;

    // Si on dépasse la taille de notre tableau, on reprend à l'index 0
    if (iPositionRailTrainElement[iPositionRailTrainElementIndex] >= railsPositions.length) {
        iPositionRailTrainElement[iPositionRailTrainElementIndex] = 0;
    }

    return railsPositions[iPositionRailTrainElement[iPositionRailTrainElementIndex]];
}

// Fonction qui permet d'afficher un objet
// export function show(object) {
//     object.traverse(function (child) {
//         if (child instanceof THREE.Mesh) {
//             child.visible = true;
//         }
//     });
// }

// Fonction qui permet de masquer un objet
// export function hide(object) {
//     object.traverse(function (child) {
//         if (child instanceof THREE.Mesh) {
//             child.visible = false;
//         }
//     });
// }

// Permet d'ajout une source de lumière
export function addLight(scene, x, y, z, color, intensity, isDynamic) {
    let light;
    light = new THREE.PointLight(color, intensity);
    light.position.set(x, y, z);
    scene.add(light);

    // Soit on augmente les valeur, soit on les diminues
    // direction (plus ou moins) permet de savoir si on a atteint la limite et qu'on doit donc aller dans l'autre sens
    let direction = 'plus';

    // Vitesse à laquelle la lumière change
    let vitesseLumiere = 0.005;
    setInterval(function () {
        if (true === isDynamic) {

            // On agit sur le ciel et la lumière si elle est dynamique (argument isDynamic = true)
            var uniforms = sky.material.uniforms;
            if (direction === 'plus') {
                light.intensity += vitesseLumiere;
                // uniforms["luminance"].value += 0.1;
                // uniforms["rayleigh"].value += 0.1;
            } else if (direction === 'moins') {
                light.intensity -= vitesseLumiere;
                // uniforms["luminance"].value -= 0.1;
                // uniforms["rayleigh"].value -= 0.1;
            }

            // if (uniforms["rayleigh"].value < 0.1) {
            //     uniforms["rayleigh"].value = 0.1;
            // }
            //
            // if (uniforms["luminance"].value > 1) {
            //     uniforms["luminance"].value = 1;
            // }
            //
            // if (uniforms["luminance"].value < 0.1) {
            //     uniforms["luminance"].value = 0.1;
            // }
        }

        if (light.intensity > 0.5) {
            direction = 'moins';
        }

        if (light.intensity > 0.3) {
            light.color.setHex(0xFFB1A0);
        }

        if (light.intensity < 0.3) {
            light.color.setHex(0xffffff);
        }

        if (light.intensity < 0.1) {
            direction = 'plus';
        }
    }, 200);
}

// https://threejs.org/docs/#api/en/audio/Audio
export function playSound(camera, stateTrain, filepath, loop, volume, isDependForTrain) {
    var listener = new THREE.AudioListener();
    camera.add(listener);
    var sound = new THREE.Audio(listener);
    var audioLoader = new THREE.AudioLoader();

    audioLoader.load(filepath, function (buffer) {
        sound.setBuffer(buffer);
        sound.setLoop(loop);
        sound.setVolume(volume);
        sound.play();

        // Si le son en question dépend de l'état du train
        if (true === isDependForTrain) {
            setInterval(function () {

                // Si train à l'arrêt on stop le son
                if (0 === stateTrain.state) {
                    sound.setVolume(0);
                } else if (1 === stateTrain.state) {
                    sound.setVolume(volume);
                }
            }, 1);
        }
    });
}

// Methode appelé au moment du chargement depuis un loader
var onProgress = function (xhr) {
    // Affichage de la progression du changement dans la console du navigateur
    if (xhr.lengthComputable) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
};

// Methode appelé au moment d'une erreur depuis un loader
var onError = function () {
};

export function addObject(path, name, fileNameMtl, fileNameObject, fileNameTexture, callback) {
    if (null === fileNameMtl) {
        objLoader = new OBJLoader(manager);
        objLoader.setPath(path);
        objLoader.load(fileNameObject, function (template) {
            template.name = name;

            // Je charge une texture dans textureLoader à l'aide de TextureLoader()
            // https://threejs.org/docs/#api/en/loaders/TextureLoader
            let textureLoader = new THREE.TextureLoader();
            textureLoader.setPath(path);

            // Ici on crée un Mesh vierge, dans lequel on vient mettre la texture
            // https://threejs.org/docs/#api/en/materials/MeshLambertMaterial
            // Donc "Mesh" est : géométrie + material (texture)
            let materialObj = new THREE.MeshLambertMaterial({
                map: textureLoader.load(fileNameTexture)
            });

            // Je traverse les éléments du template (objet original)
            template.traverse(function (child) {

                // Si l'élément traversé est bien de type Mesh
                if (child instanceof THREE.Mesh) {

                    // J'attribue à la propriété material le Mesh créé juste au dessus (MeshLambertMaterial)
                    child.material = materialObj;
                }
            });

            // J'appel la fonction callback() passée en paramètre,
            // et je lui donne en premier argument notre objet template tout configuré
            callback(template);
        }, onProgress, onError);
    } else {
        mtlLoader = new MTLLoader(manager);
        mtlLoader.setPath(path);
        mtlLoader.load(fileNameMtl, function (materials) {
            materials.preload();
            objLoader = new OBJLoader(manager);
            objLoader.setPath(path);
            objLoader.setMaterials(materials);
            objLoader.load(fileNameObject, function (template) {
                template.name = name;

                // Je charge une texture dans textureLoader à l'aide de TextureLoader()
                // https://threejs.org/docs/#api/en/loaders/TextureLoader
                let textureLoader = new THREE.TextureLoader();
                textureLoader.setPath(path);

                // Ici on crée un Mesh vierge, dans lequel on vient mettre la texture
                // https://threejs.org/docs/#api/en/materials/MeshLambertMaterial
                // Donc "Mesh" est : géométrie + material (texture)
                let materialObj = new THREE.MeshLambertMaterial({
                    map: textureLoader.load(fileNameTexture)
                });

                // Je traverse les éléments du template (objet original)
                template.traverse(function (child) {

                    // Si l'élément traversé est bien de type Mesh
                    if (child instanceof THREE.Mesh) {

                        // J'attribue à la propriété material le Mesh créé juste au dessus (MeshLambertMaterial)
                        child.material = materialObj;
                    }
                });

                // J'appel la fonction callback() passée en paramètre,
                // et je lui donne en premier argument notre objet template tout configuré
                callback(template);
            }, onProgress, onError);
        });
    }
}

// Genère un nombre aléatoire
export function getRandomNumber() {
    return Math.floor(Math.random() * 1000) - Math.floor(Math.random() * 1000)
}

// Genère un nombre aléatoire positif
export function getRandomPositiveNumber() {
    return Math.floor(Math.random() * 1000);
}

// Genère un nombre aléatoire positifi entre 2 chiffres
export function getRandomPositiveNumberBetween(min, max) {
    let number = Math.floor(Math.random() * 1000);

    while (number < min) {
        number = getRandomPositiveNumberBetween(min, max);
    }

    while (number > max) {
        number = getRandomPositiveNumberBetween(min, max);
    }

    return number;
}

/**
 * Mode d'edition
 *
 * @param raycaster
 * @param mouse
 * @param camera
 * @param movable
 * @param movableObject
 */
export function editMode(raycaster, mouse, camera, movable, movableObject) {
    // On click sur les rails
    // https://threejs.org/docs/#api/en/core/Raycaster
    window.addEventListener('click', function (event) {
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);
        var intersects = raycaster.intersectObjects(movable, true);
        if (intersects.length > 0) {
            movableObject = intersects[0].object;
            movableObject.speed = 10;
        }
    }, false);

    // Grâce au site https://keycode.info/ nous pouvons connaitre le keycode associé à une touche
    document.addEventListener("keydown", function (event) {
        let keyCode = event.which;

        if (null !== movableObject) {
            // Speed mode
            if (keyCode === 80) { // plus vite (p)
                if (movableObject.speed <= 1) {
                    movableObject.speed += 0.01;
                } else {
                    movableObject.speed += 1;
                }

                console.log('Vitesse : ' + movableObject.speed);
            } else if (keyCode === 77) { // moins vite (m)
                if (movableObject.speed <= 1) {
                    movableObject.speed -= 0.01;
                } else {
                    movableObject.speed -= 1;
                }

                if (movableObject.speed <= 0) {
                    movableObject.speed = 0.01;
                }

                console.log('Vitesse : ' + movableObject.speed);
            }

            // Move direction
            let objectx = movableObject.position.x,
                objecty = movableObject.position.y,
                objectz = movableObject.position.z;

            if (keyCode === 40) { // haut
                objectx -= movableObject.speed;
            }
            if (keyCode === 38) { // bas
                objectx += movableObject.speed;
            }
            if (keyCode === 37) { // gauche
                objectz -= movableObject.speed;
            }
            if (keyCode === 39) { // droite
                objectz += movableObject.speed;
            }

            movableObject.position.setX(objectx);
            movableObject.position.setY(objecty);
            movableObject.position.setZ(objectz);

            // Rotation sur l'axe Y (y)
            if (keyCode === 89) {
                rotateObject(movableObject, 0, 90, 0);
                movableObject.userData.numberRotation++;
            }

            // Reset (r)
            if (keyCode === 82) {
                movableObject.position.set(0, 0, 0);
            }

            // Validate (v)
            if (keyCode === 86) {
                console.log(movableObject);

                let x = movableObject.matrixWorld.getPosition().x.toFixed(2);
                let y = movableObject.matrixWorld.getPosition().y.toFixed(2);
                let z = movableObject.matrixWorld.getPosition().z.toFixed(2);
                let text = 'x:' + x;
                text += ', y:' + y;
                text += ', z:' + z;

                alert(text);
            }
        }

        // Connaitre
        if (keyCode === 67) { // touche "c"
            console.log(JSON.stringify(camera.matrix.toArray()));
        }
    }, false);
}